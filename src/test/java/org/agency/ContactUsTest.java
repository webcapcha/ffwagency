package org.agency;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;
import org.junit.Test;
import org.openqa.selenium.By;

public class ContactUsTest {

    @Test
    //("Simple test to fill subscribe form")
    public void fillSignUpFormPositive() throws Exception {
        open("https://ffwagency.com/contact");
        $(By.xpath("(//*[@id=\"Email\"])[2]")).setValue("qqq@mail.com");
        $(By.xpath("//*[@id=\"mktoForm_1023\"]/div[12]/span/button")).click();
        $(By.xpath("/html/body/div[4]/div[1]/div[1]/div[1]/div/div/h1/span")).shouldHave(text("Thank You For Subscribing to our Newsletter!"));
    }

    @Test
    //("Simple test to check negotive tooltip")
    public void fillSignUpFormNegotive() throws Exception {
        open("https://ffwagency.com/contact");
        $(By.xpath("(//*[@id=\"Email\"])[2]")).setValue("Wrong email");
        $(By.xpath("//*[@id=\"mktoForm_1023\"]/div[12]/span/button")).click();
        Thread.sleep(2000);
        $(By.cssSelector(".mktoErrorMsg")).shouldHave(text("Must be valid email.\n" + "example@yourdomain.com\n"));
    }
}
