package org.agency;

import org.junit.Test;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class GetInTouchTest {

    @Test
    //("Simple test to fill sign up form and check that success page is opened")
    public void fillSignUpFormPositive() throws Exception {
        open("https://ffwagency.com/contact");
        $(By.id("FirstName")).setValue("First Name");
        $(By.id("LastName")).setValue("Last Name");
        $(By.id("Company")).setValue("My Company");
        $(By.id("Phone")).setValue("1234567890");
        $(By.xpath("(//*[@id=\"Email\"])[1]")).setValue("qqq@mail.com");
        $(By.id("Country")).selectOption("Ukraine");
        $(By.id("commentCapture")).setValue("Message");
        $(By.xpath("//*[@id=\"mktoForm_1008\"]/div[19]/span/button")).click();
        $(By.cssSelector(".thanks-page-hero--caption > h5")).shouldHave(text("Thanks for contacting us! We will be back in touch with you soon."));
    }

    @Test
    //("Simple test to check negotive tooltip for form")
    public void fillSignUpFormNegotive() throws Exception {
        open("https://ffwagency.com/contact");
        $(By.id("FirstName")).setValue("First Name");
        $(By.id("Company")).setValue("My Company");
        $(By.id("Phone")).setValue("1234567890");
        $(By.xpath("(//*[@id=\"Email\"])[1]")).setValue("qqq@mail.com");
        $(By.id("Country")).selectOption("Ukraine");
        $(By.id("commentCapture")).setValue("Message");
        $(By.xpath("//*[@id=\"mktoForm_1008\"]/div[19]/span/button")).click();
        $(By.cssSelector(".mktoErrorMsg")).shouldHave(text("This field is required."));
    }
}
