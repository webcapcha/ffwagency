# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Using any Automation technique and tools You are familiar with create automation
scripts for Contact Us and Sign Up webforms on https://ffwagency.com/contact Script
should include negative and positive test cases.

### How do I get set up? ###

* Please pull source code to your PC
* Make sure you have already installed maven and chrome driver

### How I can run tests? ###

* You can run tests using next command

```
#!bash

mvn clean test -Dbrowser=org.openqa.selenium.chrome.ChromeDriver
```